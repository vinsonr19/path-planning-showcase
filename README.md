# path-planning-showcase

# Grid-Maze_Solvers
To view, run main.py

## Wall_Maker:

Left Click to fill in a grid point

Right Click to fill in a 3x3 block of grid points

Middle Click to open a grid point

Press Q to fill in a portion of grid points randomly

Press Esc to end the program

Press R to reset the grid

Press 1 to run Ill_Informed_Alg

Press 2 to run Slightly_Less_Ill_Informed_Alg

Press 3 to run Dijkstras

Press 4 to run A_Star


## While an algorithm is running:

Press R to restart the algorithm.

Press Space to pause/resume the algorithm.

Once it has finished, press R to reset and go back to Wall_Maker


# Continuous-Space
To view, run main.py

The algorithms (in algorithms.py) have many parameters that are customizable
and it would be fun playing with them yourself to see different results!

## create_space:

Left Click to choose two corners of a rectangle or two points on a circle

Press Space to change between making rectangles and circles

Press 1 to run RRT

Press 2 to run Probablistic_Roadmap with uniformly random sampling

Press 3 to run Probablistic_Roadmap with halton points

Press 4 to run Probablistic_Roadmap with halton points with noise

## pre_made scenarios:

While creating your obstacles (may or may not work with different size screens):

Press Q to load the floor plan

Press W to load the corridor

Press E to load a single small door

Press R to load two small doors
