from pygame.event import get
from pygame.mouse import get_pos, get_pressed
from pygame.display import flip
from pygame.locals import K_SPACE, K_ESCAPE, KEYDOWN, QUIT, K_0, K_1, K_2, K_3, K_4, K_5, K_6, K_7, K_8, K_9, K_r, K_q
from numpy.random import randint
from numpy import infty, array
from math import sqrt
from queue import PriorityQueue
from constants import *



# This algorithm moves randomly until we get to the end.  Clearly the optimal
# path wouldn't cross itself, so anytime that happens we just reset
def Ill_Informed_Alg(grid, start_pixel, end_pixel):
    running = True
    solved = False
    keep_going = False
    cur_pos = BEGINNING
    stop_searching = False
    move_list = []
    while running:
        if not stop_searching:
            if cur_pos == END:
                stop_searching = True

            else:
                legal_moves = []
                for i,j in MOVES:
                    # if we haven't visited this state (and it exists)
                    if 0<=cur_pos[0]+i<WIDTH and 0<=cur_pos[1]+j<HEIGHT:
                        if grid[cur_pos[0]+i][cur_pos[1]+j].state in [0] or cur_pos == END:
                            legal_moves.append([cur_pos[0]+i, cur_pos[1]+j])

                if len(legal_moves) == 0:
                    cur_pos = BEGINNING
                    for i,j in move_list:
                        grid[i][j].change_state(0)
                    move_list = []

                else:
                    i = randint(0, len(legal_moves))
                    cur_pos = legal_moves[i]
                    if cur_pos != END:
                        grid[cur_pos[0]][cur_pos[1]].change_state(3)
                    move_list.append(cur_pos)

        if solved:
            for i in range(WIDTH):
                for j in range(HEIGHT):
                    if grid[i][j].state not in [-1, 0, 1]:
                        grid[i][j].change_state(0)
            grid[END[0]][END[1]].change_state(1)
            for i,j in move_list:
                grid[i][j].change_state(3)

                screen.fill(BLACK)

                for i in range(WIDTH):
                    for j in range(HEIGHT):
                        grid[i][j].show()

                start_pixel.show()
                end_pixel.show()

                flip()

            solved = False


        events = get()
        for event in events:
            if event.type == KEYDOWN:
                if event.key == K_SPACE:
                    stop_searching = not stop_searching

                elif event.key == K_ESCAPE:
                    running = False
                    return False, grid

                elif event.key == K_r:
                    cur_pos = BEGINNING
                    for i,j in move_list:
                        grid[i][j].change_state(0)
                    move_list = []

        screen.fill(BLACK)

        for i in range(WIDTH):
            for j in range(HEIGHT):
                grid[i][j].show()

        start_pixel.show()
        end_pixel.show()

        flip()

    return False, grid


# This algorithm moves randomly until we get to the end.  This time, however,
# if we come across a point we've already seen we update our path
def Slightly_Less_Ill_Informed_Alg(grid, start_pixel, end_pixel):
    running = True
    solved = False
    keep_going = False
    cur_pos = BEGINNING
    stop_searching = False
    move_list = []
    while running:
        if not stop_searching:
            if cur_pos == END:
                stop_searching = True

            else:
                legal_moves = []
                for i,j in MOVES:
                    # if we haven't visited this state (and it exists)
                    if 0<=cur_pos[0]+i<WIDTH and 0<=cur_pos[1]+j<HEIGHT:
                        if grid[cur_pos[0]+i][cur_pos[1]+j].state in [0,3] or cur_pos == END:
                            legal_moves.append([cur_pos[0]+i, cur_pos[1]+j])

                i = randint(0, len(legal_moves))
                cur_pos = legal_moves[i]

                if cur_pos == BEGINNING:
                    for i,j in move_list[1:]:
                        grid[i][j].change_state(0)
                    move_list = []

                elif grid[cur_pos[0]][cur_pos[1]].state == 3:
                    index = move_list.index(cur_pos)
                    for i in range(index+1, len(move_list)):
                        grid[move_list[i][0]][move_list[i][1]].change_state(0)
                    move_list = move_list[:index + 1]

                elif cur_pos != END:
                    grid[cur_pos[0]][cur_pos[1]].change_state(3)
                    move_list.append(cur_pos)

        if solved:
            for i in range(WIDTH):
                for j in range(HEIGHT):
                    if grid[i][j].state not in [-1, 0, 1]:
                        grid[i][j].change_state(0)
            grid[END[0]][END[1]].change_state(1)
            for i,j in move_list:
                grid[i][j].change_state(3)

                screen.fill(BLACK)

                for i in range(WIDTH):
                    for j in range(HEIGHT):
                        grid[i][j].show()

                start_pixel.show()
                end_pixel.show()

                flip()

            solved = False

        events = get()
        for event in events:
            if event.type == KEYDOWN:
                if event.key == K_SPACE:
                    stop_searching = not stop_searching

                elif event.key == K_ESCAPE:
                    running = False
                    return False, grid

                elif event.key == K_r:
                    cur_pos = BEGINNING
                    for i,j in move_list:
                        grid[i][j].change_state(0)
                    move_list = []

        screen.fill(BLACK)

        for i in range(WIDTH):
            for j in range(HEIGHT):
                grid[i][j].show()

        start_pixel.show()
        end_pixel.show()

        flip()

    return False, grid


# This algorithm is a famous breadth-first search algorithm that chooses to
# search the closest node to the beginning node at all times
def Dijkstras(grid, start_pixel, end_pixel):
    # This returns the current quickest path from BEGINNING to the current pos
    def reconstruct_path(camefrom, cur):
        if cur != END and cur != BEGINNING:
            total_path = [cur]
        else:
            total_path = []
        while camefrom[cur[0]][cur[1]] != None:
            cur = camefrom[cur[0]][cur[1]]
            if cur != END and cur != BEGINNING:
                total_path.insert(0, cur)
        return total_path

    # Returns which neighbors are viable
    def neighbor_getter(pos):
        legal_moves = []
        for i,j in MOVES:
            # if we haven't visited this state (and it exists)
            if 0<=pos[0]+i<WIDTH and 0<=pos[1]+j<HEIGHT:
                # if grid[pos[0]+i][pos[1]+j].state != -1 and [pos[0]+i, pos[1]+j] != BEGINNING:
                if grid[pos[0]+i][pos[1]+j].state in [0, 1, 3]:
                    legal_moves.append([pos[0]+i, pos[1]+j])
        return legal_moves

    # keep track of when we update nodes
    count = 0

    # PriorityQueue is an efficient data structure for this and will hold
    # all nodes that are being used
    open_set = PriorityQueue()
    open_set.put((0, -count, BEGINNING))

    # stores whether point [i,j] is currently in the PriorityQueue, which can't
    # efficiently do this
    in_open_set = [[False for j in range(HEIGHT)] for i in range(WIDTH)]
    in_open_set[BEGINNING[0]][BEGINNING[1]] = True

    # Stores the previous node in the best route to point [i,j]
    came_from = [[None for j in range(HEIGHT)] for i in range(WIDTH)]

    # best_score contains the best current cost to get to point [i,j]
    best_score = [[infty for j in range(HEIGHT)] for i in range(WIDTH)]
    best_score[BEGINNING[0]][BEGINNING[1]] = 0

    running = True # outer loop
    solving = True # whether we are actively solving the maze
    solved = False # whether we have solved the maze
    while running:
        if solving:
            if open_set.empty():
                print('There is no path from beginning to end.')
                solving = False
                return True, grid

            current = open_set.get()[2]
            in_open_set[current[0]][current[1]] = False

            # if we're at the end, return the best path
            if current == END:
                move_list = reconstruct_path(came_from, current)
                solving = False
                solved = True

            neighbors = neighbor_getter(current)
            for neighbor in neighbors:
                # check cost at new point
            	temp_best_score = best_score[current[0]][current[1]] + 1
                # if better
            	if temp_best_score < best_score[neighbor[0]][neighbor[1]]:
                    # update came_from and best_score
            		came_from[neighbor[0]][neighbor[1]] = current
            		best_score[neighbor[0]][neighbor[1]] = temp_best_score
                    # if we haven't checked the new point yet, put in open_set
            		if not in_open_set[neighbor[0]][neighbor[1]]:
            			count += 1
            			open_set.put((best_score[neighbor[0]][neighbor[1]], -count, neighbor))
            			in_open_set[neighbor[0]][neighbor[1]] = True
            			grid[neighbor[0]][neighbor[1]].change_state(3)

            if current not in [BEGINNING, END]:
                grid[current[0]][current[1]].change_state(2)


        elif solved:
            for i in range(WIDTH):
                for j in range(HEIGHT):
                    if grid[i][j].state not in [-1, 0, 1]:
                        grid[i][j].change_state(0)
            grid[END[0]][END[1]].change_state(1)
            for i,j in move_list:
                grid[i][j].change_state(3)

                screen.fill(BLACK)

                for i in range(WIDTH):
                    for j in range(HEIGHT):
                        grid[i][j].show()

                start_pixel.show()
                end_pixel.show()

                flip()

            solved = False

        events = get()
        for event in events:
            if event.type == KEYDOWN:
                if event.key == K_SPACE:
                    stop_searching = not stop_searching

                elif event.key == K_ESCAPE:
                    running = False
                    return False, grid

                elif event.key == K_r:
                    return True, grid


        screen.fill(BLACK)

        for i in range(WIDTH):
            for j in range(HEIGHT):
                grid[i][j].show()

        start_pixel.show()
        end_pixel.show()

        flip()

    return False, grid


# A_Star is an algorithm based on Dijkstra's Algorithm with a heuristic that
# tells us how to choose which node in the open set is most worth checking by
# utilizing a function that guesses how far away from the end each node is
# in order to do a "best-first" search
# https://en.wikipedia.org/wiki/A*_search_algorithm
def A_Star(grid, start_pixel, end_pixel):
    # This returns the current quickest path from BEGINNING to the current pos
    def reconstruct_path(camefrom, cur):
        if cur != END and cur != BEGINNING:
            total_path = [cur]
        else:
            total_path = []
        while camefrom[cur[0]][cur[1]] != None:
            cur = camefrom[cur[0]][cur[1]]
            if cur != END and cur != BEGINNING:
                total_path.insert(0, cur)
        return total_path

    # Returns which neighbors are viable
    def neighbor_getter(pos):
        legal_moves = []
        for i,j in MOVES:
            # if we haven't visited this state (and it exists)
            if 0<=pos[0]+i<WIDTH and 0<=pos[1]+j<HEIGHT:
                # if grid[pos[0]+i][pos[1]+j].state != -1 and [pos[0]+i, pos[1]+j] != BEGINNING:
                if grid[pos[0]+i][pos[1]+j].state in [0, 1, 2, 3, 4]:
                    legal_moves.append([pos[0]+i, pos[1]+j])
        return legal_moves

    # function to approximate distance to goal
    h = lambda cur_pos: abs(cur_pos[0]-END[0]) + abs(cur_pos[1]-END[1])

    # keep track of when we update nodes
    count = 0

    # PriorityQueue is an efficient data structure for this and will hold
    # all nodes that are being used
    open_set = PriorityQueue()
    open_set.put((0, -count, BEGINNING))

    # stores whether point [i,j] is currently in the PriorityQueue, which can't
    # efficiently do this
    in_open_set = [[False for j in range(HEIGHT)] for i in range(WIDTH)]
    in_open_set[BEGINNING[0]][BEGINNING[1]] = True

    # Stores the previous node in the best route to point [i,j]
    came_from = [[None for j in range(HEIGHT)] for i in range(WIDTH)]

    # g_score contains the best current cost to get to point [i,j]
    g_score = [[infty for j in range(HEIGHT)] for i in range(WIDTH)]
    g_score[BEGINNING[0]][BEGINNING[1]] = 0

    # f_score contains the best current estimate to get from beginning to end
    # when going through point [i,j]
    f_score = [[0 for j in range(HEIGHT)] for i in range(WIDTH)]
    f_score[BEGINNING[0]][BEGINNING[1]] = h(BEGINNING)

    running = True
    solving = True
    solved = False
    prev_best_path_guess = [] # for showing current best path
    while running:
        if solving:
            if open_set.empty():
                print('There is no path from beginning to end.')
                solving = False
                return True, grid

            current = open_set.get()[2]
            in_open_set[current[0]][current[1]] = False

            if current == END:
                move_list = reconstruct_path(came_from, current)
                solving = False
                solved = True

            neighbors = neighbor_getter(current)
            for neighbor in neighbors:
            	temp_g_score = g_score[current[0]][current[1]] + 1
                # if current best cost to neighbor is cheapest thus far
            	if temp_g_score < g_score[neighbor[0]][neighbor[1]]:
            		came_from[neighbor[0]][neighbor[1]] = current
            		g_score[neighbor[0]][neighbor[1]] = temp_g_score
            		f_score[neighbor[0]][neighbor[1]] = temp_g_score + h(neighbor)
            		if not in_open_set[neighbor[0]][neighbor[1]]:
            			count += 1
            			open_set.put((f_score[neighbor[0]][neighbor[1]], -count, neighbor))
            			in_open_set[neighbor[0]][neighbor[1]] = True
            			grid[neighbor[0]][neighbor[1]].change_state(3)

            if current not in [BEGINNING, END]:
                grid[current[0]][current[1]].change_state(2)

            # if we're not done solving, show the current best guess
            if not solved:
                cur_best_path = reconstruct_path(came_from, current)
                for i,j in prev_best_path_guess:
                    if [i,j] not in cur_best_path:
                        grid[i][j].change_state(2)
                for i,j in cur_best_path:
                    grid[i][j].change_state(4)
                prev_best_path_guess = cur_best_path.copy()


            else:
                for i,j in prev_best_path_guess:
                    if grid[i][j].state == 4:
                        grid[i][j].change_state(2)



        elif solved:
            for i in range(WIDTH):
                for j in range(HEIGHT):
                    if grid[i][j].state not in [-1, 0, 1]:
                        grid[i][j].change_state(0)
            grid[END[0]][END[1]].change_state(1)
            for i,j in move_list:
                grid[i][j].change_state(3)

                screen.fill(BLACK)

                for i in range(WIDTH):
                    for j in range(HEIGHT):
                        grid[i][j].show()

                start_pixel.show()
                end_pixel.show()

                flip()

            solved = False

        events = get()
        for event in events:
            if event.type == KEYDOWN:
                if event.key == K_SPACE:
                    stop_searching = not stop_searching

                elif event.key == K_ESCAPE:
                    running = False
                    return False, grid

                elif event.key == K_r:
                    return True, grid


        screen.fill(BLACK)

        for i in range(WIDTH):
            for j in range(HEIGHT):
                grid[i][j].show()

        start_pixel.show()
        end_pixel.show()

        flip()

    return False, grid
