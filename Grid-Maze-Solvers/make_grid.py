from pygame.event import get
from pygame.mouse import get_pos, get_pressed
from pygame.display import flip
from pygame.locals import K_SPACE, K_ESCAPE, KEYDOWN, QUIT, K_0, K_1, K_2, K_3, K_4, K_5, K_6, K_7, K_8, K_9, K_r, K_q
from constants import *
from pixel_class import Pixel
from numpy.random import binomial

def Wall_Maker(grid = None):
    # what happens when we click
    def click(pos):
        x = pos[0] // (SCREEN_WIDTH // WIDTH) # find x coord
        y = pos[1] // (SCREEN_HEIGHT // HEIGHT) # find y coord
        if 0 <= x < WIDTH and 0 <= y < HEIGHT: # if the click was on the screen
            cur_pixel = grid[x][y]
            if cur_pixel != start_pixel and cur_pixel != end_pixel:
                cur_pixel.change_state(-1)

    # what happens when we middleclick
    def unclick(pos):
        x = pos[0] // (SCREEN_WIDTH // WIDTH) # find x coord
        y = pos[1] // (SCREEN_HEIGHT // HEIGHT) # find y coord
        if 0 <= x < WIDTH and 0 <= y < HEIGHT: # if the click was on the screen
            cur_pixel = grid[x][y]
            if cur_pixel != start_pixel and cur_pixel != end_pixel:
                cur_pixel.change_state(0)

    # what happens when we right click (change a 3x3 block at a time)
    def rightclick(pos):
        x = pos[0] // (SCREEN_WIDTH // WIDTH)
        y = pos[1] // (SCREEN_HEIGHT // HEIGHT)
        if 0 <= x < WIDTH and 0 <= y < HEIGHT: # if the click was on the screen
            for i in range(max(x-1, 0), min(x+2, WIDTH)): # prevents trying to change things that are off screen
                for j in range(max(y-1, 0), min(y+2, HEIGHT)): # prevents trying to change things that are off screen
                    cur_pixel = grid[i][j]
                    if cur_pixel != start_pixel and cur_pixel != end_pixel:
                        cur_pixel.change_state(-1)

    try:
        for i in range(WIDTH):
            for j in range(HEIGHT):
                if grid[i][j].state not in [-1, 1]:
                    grid[i][j].change_state(0)

    except:
        grid = [[Pixel(i,j) for j in range(HEIGHT)] for i in range(WIDTH)]

    start_pixel = grid[BEGINNING[0]][BEGINNING[1]]
    start_pixel.change_state(1)
    end_pixel = grid[END[0]][END[1]]
    end_pixel.change_state(1)

    running = True
    while running:
        events = get()
        for event in events:
            if get_pressed()[0]:
                try:
                    pos = get_pos()
                    click(pos)
                except AttributeError:
                    pass

            elif get_pressed()[1]:
                try:
                    pos = get_pos()
                    unclick(pos)
                except AttributeError:
                    pass

            elif get_pressed()[2]:
                try:
                    pos = get_pos()
                    rightclick(pos)
                except AttributeError:
                    pass

            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    return False, -1

                elif event.key == K_r:
                    return Wall_Maker()

                elif event.key == K_q:
                    change_list = binomial(1, 0.2, size=(WIDTH, HEIGHT))
                    for i in range(WIDTH):
                        for j in range(HEIGHT):
                            if [i,j] != BEGINNING and [i,j] != END and grid[i][j].state != -1:
                                grid[i][j].change_state(-change_list[i][j])

                elif event.key == K_1:
                    return True, 1, grid, start_pixel, end_pixel

                elif event.key == K_2:
                    return True, 2, grid, start_pixel, end_pixel

                elif event.key == K_3:
                    return True, 3, grid, start_pixel, end_pixel

                elif event.key == K_4:
                    return True, 4, grid, start_pixel, end_pixel

            elif event.type == QUIT:
                return False, -1

        # draw what needs to be drawn
        screen.fill(BLACK)
        for i in range(WIDTH):
            for j in range(HEIGHT):
                grid[i][j].show()
        start_pixel.show()
        end_pixel.show()
        flip()
