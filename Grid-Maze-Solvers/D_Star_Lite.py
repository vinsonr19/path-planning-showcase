from pygame.event import get
from pygame.mouse import get_pos, get_pressed
from pygame.display import flip
from pygame.locals import K_SPACE, K_ESCAPE, KEYDOWN, QUIT, K_0, K_1, K_2, K_3, K_4, K_5, K_6, K_7, K_8, K_9, K_r, K_q
from numpy.random import randint
from numpy import infty, array
from math import sqrt
from queue import PriorityQueue
from constants import *

# D_Star (D*) is an algorithm is a path planning algorithm with looser constraints
# than A*.  Possibly the most useful is that D* doesn't require that we know
# the position of all obstacles ahead of time. view_dist=3 means that we don't
# see an obstacle until we are with 3 blocks of it.
# For this algorithm, after pressing 5, you may left click to add obstacles
# that are not visible to the algorithm initially. middle click still frees the
# point while right click now does a single visible obstacle. Note that pressing
# Q will now fill the area with this new type of obstacle
# Press SPACE to begin the search.
# https://en.wikipedia.org/wiki/D*
# http://idm-lab.org/bib/abstracts/papers/aaai02b.pdf
# https://github.com/mdeyo/d-star-lite/blob/master/d_star_lite.py
def D_Star_Lite(grid, start_pixel, end_pixel, view_dist=3):
    # what happens when we click
    def click(pos):
        x = pos[0] // (SCREEN_WIDTH // WIDTH) # find x coord
        y = pos[1] // (SCREEN_HEIGHT // HEIGHT) # find y coord
        if 0 <= x < WIDTH and 0 <= y < HEIGHT: # if the click was on the screen
            cur_pixel = grid[x][y]
            if cur_pixel != start_pixel and cur_pixel != end_pixel:
                cur_pixel.change_state(-2)

    # what happens when we middleclick
    def unclick(pos):
        x = pos[0] // (SCREEN_WIDTH // WIDTH) # find x coord
        y = pos[1] // (SCREEN_HEIGHT // HEIGHT) # find y coord
        if 0 <= x < WIDTH and 0 <= y < HEIGHT: # if the click was on the screen
            cur_pixel = grid[x][y]
            if cur_pixel != start_pixel and cur_pixel != end_pixel:
                cur_pixel.change_state(0)

    # what happens when we right click
    def rightclick(pos):
        x = pos[0] // (SCREEN_WIDTH // WIDTH) # find x coord
        y = pos[1] // (SCREEN_HEIGHT // HEIGHT) # find y coord
        if 0 <= x < WIDTH and 0 <= y < HEIGHT: # if the click was on the screen
            cur_pixel = grid[x][y]
            if cur_pixel != start_pixel and cur_pixel != end_pixel:
                cur_pixel.change_state(-1)

    def reconstruct_path(camefrom, cur):
        if cur != END and cur != BEGINNING:
            total_path = [cur]
        else:
            total_path = []
        while camefrom[cur[0]][cur[1]] != None:
            cur = camefrom[cur[0]][cur[1]]
            if cur != END and cur != BEGINNING:
                total_path.insert(0, cur)
        return total_path

    def neighbor_getter(pos):
        legal_moves = []
        for i,j in MOVES:
            # if we haven't visited this state (and it exists)
            if 0<=pos[0]+i<WIDTH and 0<=pos[1]+j<HEIGHT:
                # if grid[pos[0]+i][pos[1]+j].state != -1 and [pos[0]+i, pos[1]+j] != BEGINNING:
                if grid[pos[0]+i][pos[1]+j].state in [0, 1, 2, 3, 4]:
                    legal_moves.append([pos[0]+i, pos[1]+j])
        return legal_moves

    # one-step lookahead values of g_score at pos
    # works because costs of predecessors may be updated prior to the position
    # itself, allowing for potentially more accurate estimates of cost
    # (Note that ideally this is equal to our g_score,
    # called locally inconstistent, but updating g_score isn't free)
    def rhs(pos, g, camefrom):
        if pos == BEGINNING:
            return 0
        predecessor_list = reconstruct_path(camefrom, pos)
        best = infty
        for index, i,j in enumerate(predecessor_list):
            # assumes pos != END
            best = min(best, g[i][j] + len(predecessor_list) - index)
        return best

    # cost estimate between pos, new_pos
    def h(cur_pos, pos):
        return abs(cur_pos[0]-pos[0]) + abs(cur_pos[1]-pos[1])

    def Calculate_Key(pos, k_m, rhs_list):
        return [rhs_list[pos[0]][pos[1]] + h(BEGINNING, pos) + k_m, rhs_list[pos[0]][pos[1]]]


    running = True
    while running:
        events = get()
        for event in events:
            if get_pressed()[0]:
                try:
                    pos = get_pos()
                    click(pos)
                except AttributeError:
                    pass

            elif get_pressed()[1]:
                try:
                    pos = get_pos()
                    unclick(pos)
                except AttributeError:
                    pass

            elif get_pressed()[2]:
                try:
                    pos = get_pos()
                    rightclick(pos)
                except AttributeError:
                    pass

            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    return False, -1

                elif event.key == K_q:
                    change_list = binomial(1, 0.2, size=(WIDTH, HEIGHT))
                    for i in range(WIDTH):
                        for j in range(HEIGHT):
                            if [i,j] != BEGINNING and [i,j] != END and grid[i][j].state != -1:
                                grid[i][j].change_state(-2*change_list[i][j])

                elif event.key == K_SPACE:
                    running = False

            elif event.type == QUIT:
                return False, -1

        # draw what needs to be drawn
        screen.fill(BLACK)
        for i in range(WIDTH):
            for j in range(HEIGHT):
                grid[i][j].show()
        start_pixel.show()
        end_pixel.show()
        flip()


    # Initialize variables
    k_m = 0

    # keep track of when we update nodes
    count = 0

    # PriorityQueue is an efficient data structure for this and will hold
    # all nodes that are being used
    u_set = PriorityQueue()
    u_set.put((h(BEGINNING, END), -count, END))



    # stores whether point [i,j] is currently in the PriorityQueue, which can't
    # efficiently do this
    in_u_set = [[False for j in range(HEIGHT)] for i in range(WIDTH)]
    in_u_set[BEGINNING[0]][BEGINNING[1]] = True

    # Stores the previous node in the best route to point [i,j]
    came_from = [[None for j in range(HEIGHT)] for i in range(WIDTH)]

    # g_score contains the best current cost to get to point [i,j]
    g_score = [[infty for j in range(HEIGHT)] for i in range(WIDTH)]
    g_score[BEGINNING[0]][BEGINNING[1]] = 0

    rhs_list = [[infty for j in range(HEIGHT)] for i in range(WIDTH)]
    rhs_list[END[0]][END[1]] = 0
