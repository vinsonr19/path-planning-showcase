from pygame.display import set_mode

# Define constants
WIDTH = 70
HEIGHT = 50
GRID_PIXEL_SIZE = 15
SPACE_BETWEEN_PIXELS = 2
TOTAL = GRID_PIXEL_SIZE+SPACE_BETWEEN_PIXELS
SCREEN_WIDTH = WIDTH*TOTAL+1
SCREEN_HEIGHT = HEIGHT*TOTAL+1
BEGINNING = [5,5]
END = [65,45]
MOVES = [[1,0], [0,1], [-1,0], [0,-1]]


# Define colors
WHITE = (255,255,255) # blank pixel
BLACK = (0,0,0) # background/wall
RED = (255,0,0) # closed set
GREEN = (0,255,0) # open set
BLUE = (0,0,255) # beginning/end
SKY_BLUE = (135, 206, 235) # best current guess for A* and D*
GRAY = (128, 128, 128) # obstacles not originally seen for D*


# Define state dict
state_dict = {0:WHITE, -1:BLACK, 1:BLUE, 2:RED, 3:GREEN, 4:SKY_BLUE, -2:GRAY}

# initialize background
screen = set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
screen.fill(BLACK)
