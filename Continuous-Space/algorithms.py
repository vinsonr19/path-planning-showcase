from numpy.random import uniform
from numpy.linalg import norm
from numpy import infty, zeros, vstack, hstack, zeros_like, argpartition, array
from math import atan2, sin, cos, floor, log2
from pygame.display import flip
from constants import *
from time import sleep
from halton import halton_sequence
from A_Star import *

# http://msl.cs.uiuc.edu/~lavalle/papers/Lav98c.pdf
# https://github.com/AtsushiSakai/PythonRobotics/blob/master/PathPlanning/RRT/rrt.py
# https://gist.github.com/Fnjn/58e5eaa27a3dc004c3526ea82a92de80
# https://en.wikipedia.org/wiki/Rapidly-exploring_random_tree
# This is an implementation of the original Rapidly-exploring Random Tree
# algorithm.  There are many different versions of this algorithm that improve
# upon RRT.
def RRT(area, x_init=BEGINNING, K=100000, goal_sample_rate=.02, step_size=WIDTH//30, path_resolution=WIDTH//100, expand_dist=WIDTH//20):
    '''
    area: Space class created in create_space.py
    x_init: initial starting place (will be BEGINNING here)
    K: max iterations
    goal_sample_rate: what proportion of the time we attempt to add the goal as a node
    step_size: the max distance moved for each new node
    path_resolution: the max distance we're allowed to "cheat" to connect to a new node
    expand_dist: controls the number of sub-steps between each node
    '''

    def get_random_node():
        if uniform(0,1) < goal_sample_rate:
            return END
        return (uniform(0, WIDTH), uniform(0,HEIGHT))

    def get_nearest_node(pos, nodes, area=area):
        index = 0
        dist = infty
        for i, node in enumerate(nodes):
            if area.dist(pos, node) < dist:
                dist = area.dist(pos, node)
                index = i
        return index

    def get_dist_angle(pos1, pos2, area=area):
        distance = area.dist(pos1, pos2)
        angle = atan2(pos1[1]-pos2[1], pos1[0]-pos2[0])
        return distance, angle

    def steer(parent_node, new_node, extend_length=infty, area=area):
        distance, theta = get_dist_angle(parent_node, new_node)

        old_node = parent_node
        new_node_path = [parent_node]
        if extend_length > distance:
            extend_length = distance

        n_expand = floor(extend_length / path_resolution)

        for _ in range(n_expand):
            delta_w = path_resolution * cos(theta)
            delta_h = path_resolution * sin(theta)
            old_node = (old_node[0] - delta_w, old_node[1] - delta_h)
            new_node_path.append(old_node)

        # if we've gotten close enough to the new node, connect the node
        distance = area.dist(old_node, new_node)
        if distance <= path_resolution:
            new_node_path.append(new_node)
            old_node = new_node

        parent = parent_node

        return new_node, new_node_path, parent


    nodes = [BEGINNING] # list of all nodes
    parents = [None] # list of parents of all nodes
    paths = [None]

    for _ in range(K):
        random_node = get_random_node()
        if area.in_obstacle(random_node):
            continue
        nearest_node_index = get_nearest_node(random_node, nodes)
        nearest_node = nodes[nearest_node_index]

        # this prevents us from trying to jump across the entire map
        direction = (random_node[0] - nearest_node[0], random_node[1] - nearest_node[1])
        length = norm(direction)
        scale = min(step_size, length) / length
        direction = (scale*direction[0], scale*direction[1])
        random_node = (nearest_node[0]+direction[0], nearest_node[1]+direction[1])

        new_node, new_node_path, parent = steer(nearest_node, random_node)#, extend_length=expand_dist)

        if area.is_legal(new_node_path, new_node):
            nodes.append(new_node)
            parents.append(nodes.index(parent))
            paths.append(new_node_path)

            area.draw_lines(new_node_path, new_node)
            flip()

        if area.dist(new_node, END) <= expand_dist:
            final_node, final_node_path, final_parent = steer(new_node, END)
            if area.is_legal(final_node_path, final_node):
                path = [END]
                index = -1
                while parents[index] is not None:
                    cur_node = nodes[index]
                    path.append(cur_node)
                    index = parents[index]
                path.append(BEGINNING)
                path.reverse()
                area.draw_lines(path, END, color=GREEN, solution=True)
                flip()
                return path
    return None

# https://www.cs.cmu.edu/~motionplanning/papers/sbp_papers/PRM/prmbasic_01.pdf
# https://en.wikipedia.org/wiki/Probabilistic_roadmap
# This is an implementation of the Probablistic Raodmap Method (PRM) using
# uniformly randomized points or the Halton Sequence
# and a component-n node adding strategy.
# (see https://webspace.science.uu.nl/~gerae101/pdf/compare.pdf)
def Probablistic_Roadmap(area, new_node_selection='uniform', min_iterations=2**7, max_iterations=2**14, neighbor_distance=WIDTH//20, neighbor_size=8):
    '''
    area: Space class created in create_space.py
    new_node_selection: whether to use uniformly random selection or the halton sequence
    min_iterations: minimum/first number of (attempts at) added nodes
    max_iterations: maximum number of (attempts at) added nodes
    neighbor_distance: how close two nodes must be to be connected
    neighbor_size: the maximum number of nodes we'll add initially
    '''

    def get_random_node(area):
        return (uniform(0, WIDTH), uniform(0,HEIGHT))

    nodes = [BEGINNING]
    old_node_length = 0
    for exponent in range(int(log2(min_iterations)), int(log2(max_iterations))):
        if new_node_selection == 'uniform':
            for _ in range(2**exponent):
                if new_node_selection == 'uniform':
                    rand_node = get_random_node(area)
                    if area.in_obstacle(rand_node):
                        continue
                    nodes.append(rand_node)
                    area.draw_point(rand_node)
                    if exponent < 10:
                        flip()
        elif new_node_selection == 'halton':
            halton_nodes = halton_sequence(2**exponent, 2)
            for pos in halton_nodes:
                if area.in_obstacle(pos):
                    halton_nodes.remove(pos)
                else:
                    area.draw_point(pos)
                    if exponent < 10:
                        flip()
            nodes += halton_nodes
            for pos in halton_nodes:
                area.draw_point(pos)
                if exponent < 10:
                    flip()

        elif new_node_selection == 'halton_noise':
            halton_nodes = halton_sequence(2**exponent, 2, var=True)
            for pos in halton_nodes:
                if area.in_obstacle(pos):
                    halton_nodes.remove(pos)
                else:
                    area.draw_point(pos)
                    if exponent < 10:
                        flip()
            nodes += halton_nodes
            for pos in halton_nodes:
                area.draw_point(pos)
                if exponent < 10:
                    flip()

        flip()
        if exponent == log2(min_iterations):
            nodes.append(END)
            adjacency_matrix = zeros((len(nodes), len(nodes)))
            cost_matrix = array([[infty for _ in range(len(nodes))] for i in range(len(nodes))])

            for index, node in enumerate(nodes):
                dist_list = [infty for _ in range(len(nodes))]
                for i in range(len(nodes)):
                    temp_node = nodes[i]
                    if temp_node != node:
                        dist_list[i] = area.dist(node, temp_node)
                index_list = argpartition(dist_list, neighbor_size)
                for i in index_list[:neighbor_size]:
                    if dist_list[i] < neighbor_distance and area.is_line_legal(nodes[index], nodes[i]):
                        cost_matrix[index, i] = dist_list[i]
                        cost_matrix[i, index] = dist_list[i]
                        adjacency_matrix[i, index] = 1
                        adjacency_matrix[index, i] = 1
                        area.draw_line(nodes[index], nodes[i])
                        if exponent < 10:
                            flip()

        else:
            adjacency_matrix = hstack((adjacency_matrix, zeros((adjacency_matrix.shape[0], len(nodes)-old_node_length))))
            adjacency_matrix = vstack((adjacency_matrix, zeros((len(nodes)-old_node_length, len(nodes)))))
            cost_matrix = hstack((cost_matrix, infty + zeros((cost_matrix.shape[0], len(nodes)-old_node_length))))
            cost_matrix = vstack((cost_matrix, infty + zeros((len(nodes)-old_node_length, len(nodes)))))

            for index, node in enumerate(nodes):
                dist_list = [infty for _ in range(len(nodes))]
                for i in range(len(nodes)):
                    temp_node = nodes[i]
                    if temp_node != node:
                        dist_list[i] = area.dist(node, temp_node)
                index_list = argpartition(dist_list, neighbor_size)
                for i in index_list[:neighbor_size]:
                    if dist_list[i] < neighbor_distance and area.is_line_legal(nodes[index], nodes[i]):
                        cost_matrix[index, i] = dist_list[i]
                        cost_matrix[i, index] = dist_list[i]
                        adjacency_matrix[i, index] = 1
                        adjacency_matrix[index, i] = 1
                        area.draw_line(nodes[index], nodes[i])
                        if exponent < 10:
                            flip()

        flip()

        solved = A_Star(adjacency_matrix, cost_matrix, nodes, area)
        if solved:
            return False, area, 2
        old_node_length = len(nodes)
